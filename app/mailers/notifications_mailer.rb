class NotificationsMailer < ActionMailer::Base

  default :from => "mailer@nivo.cl"
  default :to => I18n.t(:contact_email)

  def new_message(message)
    @message = message
    mail(:subject => "[Formulario Nivo] #{message.subject}")
  end

end
